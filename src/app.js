import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import TodoApp from './components/App';
import 'bootstrap/scss/bootstrap.scss';
import './styles/styles.scss';

const store = configureStore();

const jsx = (
  <Provider store={store}>
    <TodoApp />
  </Provider>
);

ReactDOM.render(jsx, document.getElementById('app'));
