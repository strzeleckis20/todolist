import uuid from 'uuid';

export const addItem = (
  {
    note = '',
    createdAt = 0,
    completed = false
  } = {}
) => ({
  type: 'ADD_EXPENSE',
  item: {
    id: uuid(),
    note,
    createdAt,
    completed
  }
});

export const removeItem = ({ id } = {}) => ({
  type: 'REMOVE_EXPENSE',
  id
});

export const editItem = ({id,completed}) => ({
  type: 'EDIT_EXPENSE',
  id,
  completed
});
