export const setCompleted = (completed) => ({
  type: 'SET_COMPLETED',
  completed
});
