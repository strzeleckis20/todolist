import React from 'react';
import ListItem from './ListItem';
import ListFilters from './ListFilters';
import AddItem from './AddItem';

const DashboardPage = () => (
  <div className="dashbord">
    <AddItem/>
    <ListFilters />
    <ListItem />
  </div>
);

export default DashboardPage;
