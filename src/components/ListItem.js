import React from 'react';
import { connect } from 'react-redux';
import Item from './Item';
import selectItems from '../selectors/selectItems';
import { ListGroup } from 'reactstrap';
import { removeItem, editItem } from '../actions/itemAction';

const ListItem = (props) => {

  const Click = (item) => {
    props.dispatch(removeItem({ id: item.id }))
  };
  const Checked = (item) => {
    props.dispatch(editItem({id: item.id, completed: item.completed}))
  }
  return (
  <div>
    <ListGroup>
    {props.items.map((item) => {
      return <Item key={item.id} Click={Click} Checked={Checked} {...item}/>;
    })}
    </ListGroup>
  </div>)
};

const mapStateToProps = (state) => {
  return {
    items: selectItems(state.items, state.filters)
}};

export default connect(mapStateToProps)(ListItem);
