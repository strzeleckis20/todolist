import React, { useState } from 'react';
import moment from 'moment';
import { Button, Input, InputGroupAddon, InputGroup, Form, FormFeedback } from 'reactstrap';

export default function DashboardForm(props) {

  const [note, setNote] = useState('');
  const [validator, setError] = useState(false);
  const onSubmit = (e) => {
    e.preventDefault();
    if(note.length > 0 && note.length <= 100){
      props.onSubmit({
        createdAt: moment().format("D-MM-YYYY, H:mm"),
        note: note
    });
    setNote('');
    setError(false);
  }
  else{
    setError(true);
  } 
  };

    return (
      <Form onSubmit={onSubmit}>
      <InputGroup>
          <Input 
          type="text"
          placeholder="Wprowadź zadanie"
          autoFocus
          value={note}
          onChange={e => setNote(e.target.value)}
          invalid = {validator}   
          />
         <InputGroupAddon addonType="prepend"><Button color="primary">+</Button></InputGroupAddon>
         <FormFeedback>Liczba znaków od 1 do 100</FormFeedback>
      </InputGroup>
      </Form>
    )

}
