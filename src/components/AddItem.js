import React from 'react';
import { connect } from 'react-redux';
import DashboardForm from './DashboardForm';
import { addItem } from '../actions/itemAction';

const AddItem = (props) => (
    <DashboardForm
      onSubmit={(item) => {
        props.dispatch(addItem(item));
      }}
    />
);

export default connect()(AddItem);
