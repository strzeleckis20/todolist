import React from 'react';
import { connect } from 'react-redux';
import DashboardPage from './DashboardPage';
import { Container, Row, Col } from 'reactstrap';
import ProgressBar from './Progressbar';

const TodoApp = () => (
    <Container>
    <Row>
      <Col>
      <div className="main">
      <h1>Todo List</h1>
      <ProgressBar/>
        <DashboardPage/>
        </div>
      </Col>
    </Row>
    </Container>
);

export default connect()(TodoApp);
