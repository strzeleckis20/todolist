import React from 'react';
import { connect } from 'react-redux';
import { Progress } from 'reactstrap';

const ProgressBar = (props) => {
    const precent = () => {
        if(props.value > 0)
         return (props.value / props.max * 100).toFixed(2)
        else
         return 0
    }
    return(
        <Progress color="success" value={props.value} max={props.max <= 0 ? 1 : props.max}>{precent()}%</Progress>
    )
};


const mapStateToProps = (state) => {
    return {
      max: state.items.length,
      value: state.items.filter((e) => e.completed == true).length
  }};

export default connect(mapStateToProps)(ProgressBar);