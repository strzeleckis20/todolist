import React, { useState } from 'react';
import { connect } from 'react-redux';
import { setCompleted } from '../actions/filters';

const ListFilters = (props) => {
  const [activeTab, setActiveTab] = useState('1');

  const toggle = tab => {
    if(activeTab !== tab) setActiveTab(tab);
  }
    return (
      <div className="block-filter">
         <button
         className={activeTab === '1' ? "active" : null }
          onClick={() => {
            toggle('1');
            props.dispatch(setCompleted(null))
          }}
        >Wszystkie</button>
        <button
        className={activeTab === '2' ? "active" : null }
          onClick={() => {
            toggle('2');
            props.dispatch(setCompleted(true))
          }}
        >Wykonane</button>
        <button
           className={activeTab === '3' ? "active" : null }
          onClick={() => {
            toggle('3');
            props.dispatch(setCompleted(false))
        }}
        >Niewykonane</button>
      </div>
    );
  }


const mapStateToProps = (state) => {
  return {
    filters: state.filters
  };
};

export default connect(mapStateToProps)(ListFilters);
