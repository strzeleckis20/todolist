import React from 'react';
import { ListGroupItem, Button, Input, Label } from 'reactstrap';

const Item = ({ id, Click, Checked, note, createdAt, completed }) => (
  <ListGroupItem key={id} className={completed ? "done" : null}>
      <span className="date">dodane: {createdAt}</span>
      <div>
      <Label check>
      <Input 
      type="checkbox" 
      checked={completed} 
      onChange ={() => Checked({id: id,completed: completed})}
      />
      <h3>{note}</h3>
      </Label>
      <Button 
         color="danger"
         onClick={() => Click({id})}
         disabled={completed}
        >Usuń</Button>
      </div>
  </ListGroupItem>
);

export default Item;

