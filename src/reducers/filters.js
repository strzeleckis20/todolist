const filtersReducerDefaultState = {
  completed: null
};

export default (state = filtersReducerDefaultState, action) => {
  switch (action.type) {
      case 'SET_COMPLETED':
      return {
        completed: action.completed
      };
    default:
      return state;
  }
};
