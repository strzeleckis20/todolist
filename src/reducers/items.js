
const itemsReducerDefaultState = [];

export default (state = itemsReducerDefaultState, action) => {
  switch (action.type) {
    case 'ADD_EXPENSE':
      return [
        ...state,
        action.item
      ];
    case 'REMOVE_EXPENSE':
      return state.filter(({ id }) => id !== action.id);
    case 'EDIT_EXPENSE':
      return state.map((items) => {
        if (items.id === action.id) {
          return {
            ...items,
            completed: !action.completed
          };
        } else {
          return items;
        };
      });
    default:
      return state;
  }
};
